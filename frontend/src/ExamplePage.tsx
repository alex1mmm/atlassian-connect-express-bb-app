import React, {useState, useLayoutEffect} from 'react';
import ReactDOM from 'react-dom';
import SectionMessage from '@atlaskit/section-message';
import Page, {Grid, GridColumn} from '@atlaskit/page';
import styled from 'styled-components';

const ContainerWrapper = styled.div`
  min-width: 780px;
  max-width: 780px;
  margin-top: 5%;
  margin-left: auto;
  margin-right: auto;
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
  display: block;
`;

interface IProps {
    displayName: string
    repoPath: string
}

declare let AP: any;

function ExamplePage(Props: IProps) {
    const {displayName, repoPath} = Props;
    const [apDisplayName, setApDisplayName] = useState('');
    useLayoutEffect(() => {
        AP.require('request', (request: any) => {
            request({
                url: '/2.0/user/',
                success(data: any) {
                    setApDisplayName(data.display_name);
                },
            });
        });
    });
    return (
        <ContainerWrapper>
            <SectionMessage
                title="Repository Information"
            >
                <Page>
                    <Grid>
                        <GridColumn medium={7}>Add-on user (retrieved via server-to-server REST):</GridColumn>
                        <GridColumn medium={5}><b>{displayName}</b></GridColumn>
                    </Grid>
                    <Grid>
                        <GridColumn medium={7}>Your name (retrieved via AP.request()):</GridColumn>
                        <GridColumn medium={5}><b>{apDisplayName}</b></GridColumn>
                    </Grid>
                    <Grid>
                        <GridColumn medium={7}>This repository:</GridColumn>
                        <GridColumn medium={5}><b>{repoPath}</b></GridColumn>
                    </Grid>
                    <Grid>
                        <GridColumn medium={7}>Page visits:</GridColumn>
                        <GridColumn medium={5}><b>No longer supported</b></GridColumn>
                    </Grid>
                </Page>
            </SectionMessage>
        </ContainerWrapper>
    );
}

window.addEventListener('load', () => {
    const wrapper = document.getElementById('container');
    const displayName = (document.getElementById('displayName') as HTMLInputElement).value;
    const repoPath = (document.getElementById('repoPath') as HTMLInputElement).value;
    ReactDOM.render(
        <ExamplePage
            displayName={displayName}
            repoPath={repoPath}
        />,
        wrapper,
    );
});
