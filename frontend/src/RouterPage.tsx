import React, { useState, useLayoutEffect } from 'react';
import ReactDOM from 'react-dom';
import SectionMessage from '@atlaskit/section-message';
import styled from 'styled-components';
import Page, { Grid, GridColumn } from '@atlaskit/page';
import Button from '@atlaskit/button';

const ContainerWrapper = styled.div`
  max-width: 780px;
  margin: 0 auto;
  display: block;
`;

const RepoNameContainer = styled.div`
    padding-top: 9px;
`;

const RepoSlugHeader = styled.div`
    font-weight: bold;
    padding: 10px 0 10px 10%;
`;

declare let AP: any;

export default function RouterPage() {
  const [projects, setProjects] = useState([]);
  const [project, setProject] = useState('');

  useLayoutEffect(() => {
    AP.require('request', (request: any) => {
      request({
        url: '/2.0/repositories',
        success(data: any) {
          setProjects(data.values);
        },
      });
    });
  });

  const view = () => (project === '' ? projectsView() : projectView());

  const projectsView = () => (
    projects.map((prj) => (
      <Grid>
        <GridColumn medium={5}>
          <Button appearance="link" onClick={() => setProject(prj.slug)}>{prj.slug}</Button>
        </GridColumn>
        <GridColumn medium={7}>
          <RepoNameContainer>
            {prj.full_name}
          </RepoNameContainer>
        </GridColumn>
      </Grid>

    ))
  );

  const projectView = () => {
    const currentProject = projects.filter((prj) => prj.slug === project)[0];
    return (
      <>
        <RepoSlugHeader>{currentProject.slug}</RepoSlugHeader>
        <Grid>
          <GridColumn medium={2}>created_on:</GridColumn>
          <GridColumn medium={8}>{currentProject.created_on}</GridColumn>
        </Grid>
        <Grid>
          <GridColumn medium={2}>description:</GridColumn>
          <GridColumn medium={8}>{currentProject.description}</GridColumn>
        </Grid>
        <Grid>
          <GridColumn medium={2}>fork_policy:</GridColumn>
          <GridColumn medium={8}>{currentProject.fork_policy}</GridColumn>
        </Grid>
        <Grid>
          <GridColumn medium={2}>full_name:</GridColumn>
          <GridColumn medium={8}>{currentProject.full_name}</GridColumn>
        </Grid>
        <Grid>
          <GridColumn medium={2}>has_issues:</GridColumn>
          <GridColumn medium={8}>{currentProject.has_issues}</GridColumn>
        </Grid>
        <Grid>
          <GridColumn medium={2}>has_wiki:</GridColumn>
          <GridColumn medium={8}>{currentProject.has_wiki}</GridColumn>
        </Grid>
        <Grid>
          <GridColumn medium={2}>is_private:</GridColumn>
          <GridColumn medium={8}>{currentProject.is_private}</GridColumn>
        </Grid>
        <Grid>
          <GridColumn medium={2}>language:</GridColumn>
          <GridColumn medium={8}>{currentProject.language}</GridColumn>
        </Grid>
      </>
    );
  };

  return (
    <ContainerWrapper>
      <SectionMessage
        title="Repositories"
      >
        <Page>
          {view()}
        </Page>
      </SectionMessage>
    </ContainerWrapper>
  );
}

window.addEventListener('load', () => {
  const wrapper = document.getElementById('container');
  ReactDOM.render(
    <RouterPage />,
    wrapper,
  );
});
