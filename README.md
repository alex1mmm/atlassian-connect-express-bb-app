# Used in articles
[v.1 tag] in [atlassian-connect-express: Bitbucket app]    
[v.2 tag] in [atlassian-connect-express: furl error]  
[v.3 tag] in [atlassian-connect-express: explore the source code]  
[v.4 tag] in [atlassian-connect-express 4: typescript, react and atlaskit]  
[v.5 tag] in [atlassian-connect-express 5: add parameters to config.json]  
[v.6 tag] in [atlassian-connect-express 6: debug backend in VS code]  
[v.7 tag] in [atlassian-connect-express 7: working with Postgres]  
[v.8 tag] in [atlassian-connect-express 8: execute own code after app installation]  
[v.9 tag] in [atlassian-connect-express 9: add your own settings to the database]  
[v.10 tag] in [atlassian-connect-express 10: context parameters]  

# Atlassian Add-on using Express

Congratulations! You've successfully created an Atlassian Connect App using
the Express web application framework.

# Installation

1. Install [git], [node], [npm] \(2.7.5+) and [ngrok].
2. Run `npm install`.
3. Run `ngrok http 3000` and take note of the proxy's `https://..` base url.
4. Run `AC_LOCAL_BASE_URL=https://THE_NGROK_BASE_URL node app.js` from the
repository root.

# Development loop

You can manually install/update/uninstall your add-ons from
`https://bitbucket.org/account/user/USERNAME/addon-management`.

[git]: http://git-scm.com/
[node]: https://nodejs.org/
[npm]: https://github.com/npm/npm#super-easy-install
[ngrok]: https://ngrok.com/
[v.1 tag]: https://bitbucket.org/alex1mmm/atlassian-connect-express-bb-app/src/v.1/
[v.2 tag]: https://bitbucket.org/alex1mmm/atlassian-connect-express-bb-app/src/v.2/
[v.3 tag]: https://bitbucket.org/alex1mmm/atlassian-connect-express-bb-app/src/v.3/
[v.4 tag]: https://bitbucket.org/alex1mmm/atlassian-connect-express-bb-app/src/v.4/
[v.5 tag]: https://bitbucket.org/alex1mmm/atlassian-connect-express-bb-app/src/v.5/
[v.6 tag]: https://bitbucket.org/alex1mmm/atlassian-connect-express-bb-app/src/v.6/
[v.7 tag]: https://bitbucket.org/alex1mmm/atlassian-connect-express-bb-app/src/v.7/
[v.8 tag]: https://bitbucket.org/alex1mmm/atlassian-connect-express-bb-app/src/v.8/
[v.9 tag]: https://bitbucket.org/alex1mmm/atlassian-connect-express-bb-app/src/v.9/
[v.10 tag]: https://bitbucket.org/alex1mmm/atlassian-connect-express-bb-app/src/v.10/


[atlassian-connect-express: Bitbucket app]: https://matveev.life/2021/04/17/atlassian-connect-express-bitbucket-app/  
[atlassian-connect-express: furl error]: https://matveev.life/2021/04/24/atlassian-connect-express-furl-error/  
[atlassian-connect-express: explore the source code]: https://matveev.life/2021/05/01/atlassian-connect-express-explore-the-source-code/ 
[atlassian-connect-express 4: typescript, react and atlaskit]: https://matveev.life/2021/05/09/atlassian-connect-express-4-typescript-react-and-atlaskit/
[atlassian-connect-express 5: add parameters to config.json]: https://matveev.life/2021/05/10/atlassian-connect-express-5-add-parameters-to-config-json/
[atlassian-connect-express 6: debug backend in VS code]: https://matveev.life/2021/05/16/atlassian-connect-express-6-debug-backend-in-vs-code/
[atlassian-connect-express 7: working with Postgres]: https://matveev.life/2021/05/24/atlassian-connect-express-7-working-with-postgres/
[atlassian-connect-express 8: execute own code after app installation]: https://matveev.life/2021/05/24/atlassian-connect-express-8-execute-own-code-after-app-installation/
[atlassian-connect-express 9: add your own settings to the database]: https://matveev.life/2021/05/24/atlassian-connect-express-9-add-your-own-settings-to-the-database/
[atlassian-connect-express 10: context parameters]: https://matveev.life/2021/06/06/atlassian-connect-express-10-context-parameters/
